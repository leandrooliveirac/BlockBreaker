# BlockBreaker
A basic "Breakout" style game developed with C# and Unity for learning purposes:

* Game Design
* Unity Package Management
* World Units & Play Space
* Rigidbody & Colliders
* Unity Physics Material
* Colliders, Collision & Triggers
* Move Object With Mouse
* Distance As A Vector2
* Play Space & Gravity
* Prefabs
* Physics 2D
* Basic audio feature
* Implement Singleton Pattern


## Game Features:

* Paddle controlled by mouse movement
* Multiple levels
* Basic VFX and SFX
* Multi-hit breakable blocks 
* Block durability (single or multi-hit) is used to create a more complex playfield.
* Unbreakable blocks

## How To Build / Compile
This is a Unity project, therefore it requires Unity3D to build. Clone or download this repo, and navigate to `Assets > Scenes` then open any `.unity` file.
